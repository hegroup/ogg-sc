# README #

This README documents the OGG-Sc ontology and the ogg-sc repository information. 

### What is this ogg-sc repository for? ###

* This Bitbucket repository is for OGG-Sc: the Ontology of Genes and Genomes for Saccharomyces cerevisiae (yeast). 

### What are OGG and OGG-Sc? ###

* OGG is a biological ontology in the area of genes and genomes. OGG-Sc is the OGG subset for Saccharomyces cerevisiae (yeast).

### OGG-Sc discussion ###

* Use the Bitbucket issue tracker: https://bitbucket.org/hegroup/ogg-sc/issues/ 
* General OGG discussion:
* Google OGG Discussion group email: ogg-discuss@googlegroups.com
* Google OGG Discussion group: https://groups.google.com/forum/#!forum/ogg-discuss
* Contact Oliver He, University of Michigan Medical School, Ann Arbor, MI 48105, USA: http://www.hegroup.org

### OGG in OBO ###

* [OGG in OBO library ontology website](http://obofoundry.org/ontology/ogg.html)
* [OBO Foundry](http://obofoundry.org/)

### OGG-Sc browsing ###

* [OGG-Sc in Ontobee](http://www.ontobee.org/ontology/ogg-sc)

### Other OGG subsets ###

* Additional OGG subsets are for different organisms:
* OGG: http://www.ontobee.org/ontology/OGG
* OGG-At: http://www.ontobee.org/ontology/OGG-At
* OGG-Brucella: http://www.ontobee.org/ontology/OGG-Bru
* OGG-C. elegans: http://www.ontobee.org/ontology/OGG-Ce
* OGG-Fruit Fly: http://www.ontobee.org/ontology/OGG-Dm
* OGG-Mouse: http://www.ontobee.org/ontology/OGG-Mm
* OGG-P. falciparum: http://www.ontobee.org/ontology/OGG-Pf
* OGG-Zebrafish: http://www.ontobee.org/ontology/OGG-Dr

### OGG publication ###
* He Y, Liu Y, Zhao B. OGG: a biological ontology for representing genes and genomes in specific organisms. Proceedings of the 5th International Conference on Biomedical Ontologies (ICBO), Houston, Texas, USA. October 8-9, 2014. Pages 13-20. URL: http://ceur-ws.org/Vol-1327/icbo2014_paper_23.pdf or: http://www.hegroup.org/docs/OGG-ICBO2014.pdf (with correctly formatted Figure 3).